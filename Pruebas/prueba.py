#!/usr/bin/python3
import _thread
import time
import signal, os
import sys
from telegram import (ReplyKeyboardMarkup, ReplyKeyboardRemove)
from telegram.ext import Updater, MessageHandler, Filters, CommandHandler
import requests,json
import os
#import pexpect, base64
from time import sleep
import RPi.GPIO as IO
import speech_recognition as sr

#import commands

RED = 25
GREEN = 24
BLUE = 23
IO.setmode(IO.BCM)
IO.setup(RED, IO.OUT) 
IO.setup(GREEN, IO.OUT) 
IO.setup(BLUE, IO.OUT) 
TOKEN = "505725782:AAF6Fp59BQIW6UKDDPVMSbUzvfbCF_n5Ozk"
#360783407 ---> Tablet
CHATID = ['354859685']

keyboard1 =	[
			['/temp'],
			['/ledRojo', '/ledVerde', '/ledAzul'],
			['/ledApagar'],
			['/apagar']
		]
markup1 = ReplyKeyboardMarkup(keyboard1)

# ---------------------------------------------------------------------------------------
# ---------------------------------- Metodos TelegramBot -----------------------------------
# ---------------------------------------------------------------------------------------
# Método que imprimirá por pantalla la información que reciba
def listener(bot, update):
    id = update.message.chat_id
    mensaje = update.message.text  
    print("ID: " + str(id) + " MENSAJE: " + mensaje) 

def try_recognize_audio(audio, language):
    try:
        print("Traduciendo..")
        return recognizer.recognize_google(audio, language=language).lower()
    except sr.UnknownValueError:
        print("Google Speech Recognition could not understand audio")
        pass
    except sr.RequestError as e:
        pass

def voice_handler(bot, update):
    if funcionAutorizacion(update.message.chat_id)==True:
        file = bot.getFile(update.message.voice.file_id)
        print ("file_id: " + str(update.message.voice.file_id))
        file.download('./archivos/voice.ogg')
        audio = './archivos/voice.ogg'
        file.download(audio)
        # print("You said: " + sr.Recognizer().recognize_google('./archivos/voice.ogg'))
        print(try_recognize_audio(audio, "es-AR"))

        try:
            # for testing purposes, we're just using the default API key
			# to use another API key, use `r.recognize_google(audio, key="GOOGLE_SPEECH_RECOGNITION_API_KEY")`
			# instead of `r.recognize_google(audio)`
            text1 = sr.Recognizer().recognize_google('./archivos/voice.ogg')
            print("You said: " + text1 )
            bot.sendMessage(chat_id=update.message.chat_id, text='Usuario sin permisos')
        except sr.UnknownValueError:
            print("Google Speech Recognition could not understand audio")
        except sr.RequestError as e:
            print("Could not request results from Google Speech Recognition service; {0}".format(e))
			
        print("Pasó...")
        #print("Reproduciendo audio recibido...")
		#os.system("mplayer ./archivos/voice.ogg")
		#print("Grabar audio")
		#os.system("aplay -D plughw:1 ./archivos/indicacionComienzo.wav")
		#os.system("sudo arecord --device=plughw:1,0 --duration=5 ./archivos/test.wav")
		#print("Enviando x telegram")
		#bot.sendAudio(chat_id=update.message.chat_id, audio=open('./archivos/test.wav','rb'))
    else:
        bot.sendMessage(chat_id=update.message.chat_id, text='Usuario sin permisos')

# Método de el comando de "/start"
def start(bot, update):
    if funcionAutorizacion(update.message.chat_id)==True:
        #bot.sendMessage(chat_id=update.message.chat_id, text='¡activado!, presione /menu.')
        update.message.reply_text('Seleccione la acción:',reply_markup=markup1)
    else:
        bot.sendMessage(chat_id=update.message.chat_id, text='Usuario sin permisos')
 
# Método de el comando de "/menu"
def menu(bot, update):
    if funcionAutorizacion(update.message.chat_id)==True:
        bot.sendMessage(chat_id=update.message.chat_id, text='/start\n\n /apagar\n\n /temp\n\n /ledRojo\n\n /ledVerde\n\n /ledAzul\n\n /ledApagar\n')
    else:
        bot.sendMessage(chat_id=update.message.chat_id, text='Usuario sin permisos')
	
# Método de el comando de "/temp"
def temp(bot, update):
    if funcionAutorizacion(update.message.chat_id)==True:
        #print ("Temperatura CPU: ", round(get_cpu_temp()))
        tcpu = "Temperatura CPU Raspy: " + str(round(get_cpu_temp())) + "ºC"
        bot.sendMessage(chat_id=update.message.chat_id, text=tcpu)
        IO.output(RED, IO.HIGH)
        IO.output(GREEN, IO.HIGH)
        IO.output(BLUE, IO.HIGH)
    else:
        bot.sendMessage(chat_id=update.message.chat_id, text='Usuario sin permisos')
		
def ledRojo(bot, update):
    if funcionAutorizacion(update.message.chat_id)==True:
        IO.output(GREEN, IO.LOW)
        IO.output(BLUE, IO.LOW)
        IO.output(RED, IO.HIGH)
        tcpu = "Led Rojo Encendido"
        bot.sendMessage(chat_id=update.message.chat_id, text=tcpu)
    else:
        bot.sendMessage(chat_id=update.message.chat_id, text='Usuario sin permisos')
		
def ledVerde(bot, update):
    if funcionAutorizacion(update.message.chat_id)==True:
        IO.output(RED, IO.LOW)
        IO.output(BLUE, IO.LOW)
        IO.output(GREEN, IO.HIGH)
        tcpu = "Led Verde Encendido"
        bot.sendMessage(chat_id=update.message.chat_id, text=tcpu)
    else:
        bot.sendMessage(chat_id=update.message.chat_id, text='Usuario sin permisos')

def ledAzul(bot, update):
    if funcionAutorizacion(update.message.chat_id)==True:
        IO.output(RED, IO.LOW)
        IO.output(GREEN, IO.LOW)
        IO.output(BLUE, IO.HIGH)
        tcpu = "Led Azul Encendido"
        bot.sendMessage(chat_id=update.message.chat_id, text=tcpu)
    else:
        bot.sendMessage(chat_id=update.message.chat_id, text='Usuario sin permisos')
		
def ledApagar(bot, update):
    if funcionAutorizacion(update.message.chat_id)==True:
        IO.output(RED, IO.LOW)
        IO.output(GREEN, IO.LOW)
        IO.output(BLUE, IO.LOW)
        tcpu = "Leds Apagados"
        bot.sendMessage(chat_id=update.message.chat_id, text=tcpu)
    else:
        bot.sendMessage(chat_id=update.message.chat_id, text='Usuario sin permisos')
				
# Método de el comando de "/apagar"  
def apagar(bot, update):
    if funcionAutorizacion(update.message.chat_id)==True:
        os.system("sudo shutdown -h now")
    else:
        bot.sendMessage(chat_id=update.message.chat_id, text='Usuario sin permisos')
  
# ---------------------------------------------------------------------------------------
# ---------------------------------- Metodos Handler Signals ---------------------------- 
# ---------------------------------------------------------------------------------------
def signal_handler(signum, frame):
    print('Salida por usuario.', signum)
    funcionEnviarTextoxTelegram("Apagado")
    sys.exit(1)

# ---------------------------------------------------------------------------------------
# ---------------------------------- Metodos Hilos --------------------------------------
# ---------------------------------------------------------------------------------------
def funcionAutorizacion(idparam):
    for cid in CHATID:
        if (int(idparam) == int(cid)):
            return True;
    return False;

def funcionEnviarTextoxTelegram(mensaje):
    for cid in CHATID:
        requests.post("https://api.telegram.org/bot"+TOKEN+"/sendMessage?chat_id="+cid+"&text="+mensaje)

	
def funcionhilo1( param1, param2):
    #while(1):
		time.sleep(param2)
		print("%s: %s" % ( param1, param2 ))
        #funcionEnviarTextoxTelegram("/menu")
	
def funcionhilo2( param1, param2):
    #while(1):
		time.sleep(param2)
		print("%s: %s" % ( param1, param2 ))

def funcionhilo3( param1, param2):
    #while(1):
		#time.sleep(param2)
		print("%s: %s" % ( param1, param2 ))
        #r = requests.get("https://bidesa2.herokuapp.com/wsbi/")
		#.json()
		#  print( "%s" % r.status_code )
		#  print( "%s" % r.headers )
		#print( "%s" % r.content )
		
def get_cpu_temp():
    tempFile = open( "/sys/class/thermal/thermal_zone0/temp" )
    cpu_temp = tempFile.read()
    tempFile.close()
    return float(cpu_temp)/1000

# ---------------------------------------------------------------------------------------
# ---------------------------------- Instrucciones --------------------------------------
# ---------------------------------------------------------------------------------------

def main():
    signal.signal(signal.SIGINT, signal_handler)
    _thread.start_new_thread( funcionhilo1, ("param1Hilo1", 1, ))
    _thread.start_new_thread( funcionhilo2, ("param1Hilo2", 2, ))
    _thread.start_new_thread( funcionhilo3, ("param1Hilo3", 2, ))
	
    # Creamos el Updater, objeto que se encargará de mandarnos las peticiones del bot
    updater = Updater(TOKEN)
    # Dispatcher, registraremos los comandos del bot y su funcionalidad
    dispatcher = updater.dispatcher
    # Registramos el método que hemos definido antes como listener para que muestre la información de cada mensaje
    listener_handler = MessageHandler(Filters.text, listener)
    dispatcher.add_handler(listener_handler)
    # Registramos cada método a los comandos necesarios
    dispatcher.add_handler(CommandHandler("start", start))
    dispatcher.add_handler(CommandHandler("menu", menu))
    dispatcher.add_handler(CommandHandler("temp", temp))
    dispatcher.add_handler(CommandHandler("apagar", apagar))
    dispatcher.add_handler(CommandHandler("ledApagar", ledApagar))
    dispatcher.add_handler(CommandHandler("ledRojo", ledRojo))
    dispatcher.add_handler(CommandHandler("ledVerde", ledVerde))
    dispatcher.add_handler(CommandHandler("ledAzul", ledAzul))
    dispatcher.add_handler(MessageHandler(Filters.voice, voice_handler))
    # Ejecución del bot a las peticiones
    updater.start_polling()
    updater.idle()
	
# Llamamos al método main para ejecutar lo anterior
if __name__ == '__main__':
    main()


 


